package proyectofinalprimeraevaluacioncine;

import java.util.Scanner;

public class Cine {

    Scanner scanner;
    Sala[] salas;    //inicializado salas.
    int ultimaSala;

    public Cine() {
        ultimaSala = 0;
        scanner = new Scanner(System.in);
        salas = new Sala[10];  //array con máximo de 10 salas.
    }

    public void crearSala() {

        if (ultimaSala < salas.length) {  //máximo de salas que tenga el array.
            System.out.println("Introduce el número de filas.");
            int filas = pedirEnteroPositivo();
            System.out.println("Introduce el numero de columnas.");
            int columnas = pedirEnteroPositivo();

            Sala sala = new Sala(filas, columnas);
            Pelicula pelicula = crearPelicula();
            sala.setPelicula(pelicula); // asigna la película que hemos creado a la sala.
            salas[ultimaSala] = sala;  //ejemplo: el elemento 3 de salas es el lo que has creado en este método.
            ultimaSala = ultimaSala + 1; //se le suma 1 al número de salas.
            System.out.println("Sala creada.");
        } else {
            System.err.println("Has superado el máximo de salas.");

        }
    }

    public Sala getSala(int salaHumana) {
        int salaExistente = salaHumana - 1;
        if (salaExistente <= ultimaSala) {
            return salas[salaExistente]; //encapsulamiento.
        } else {
            return null;
        }
    }

    public Integer pedirEnteroPositivo() { //para asegurarnos de que el entero recibido sea siempre más alto que 0.
        String entrada;
        Integer resultado = null;

        do {
            try {
                entrada = scanner.nextLine();
                resultado = Integer.parseInt(entrada);
            } catch (NumberFormatException e) {
                System.out.println("Introduce un número entero positivo");
            }
        } while (resultado == null || resultado <= 0);

        return resultado; //nos devuelve un integer.

    }

    public Pelicula crearPelicula() {
        // Pedir nombre
        System.out.println("Introduce el nombre de la película.");
        String nombre = scanner.nextLine();

        System.out.println("Duración en minutos de la película.");
        int duracion = pedirEnteroPositivo();

        System.out.println("Introduzca una edad mínima para ver la película.");
        int edadMinima = pedirEnteroPositivo();
        System.out.println("Pulse intro para continuar");

        Pelicula pelicula = new Pelicula();
        pelicula.setNombre(nombre); //le damos a set nombre el nombre de la peli.
        pelicula.setEdadMinima(edadMinima); //introducimos la edad minima introducida.
        pelicula.setDuracion(duracion); //introducimos la duración.
        return pelicula;   //nos devuelve la peliucla entera.
    }

    public void help() {

        System.out.println("Bienvenido a cines Adrito.");
        System.out.println("-Pulse 1 para crear una sala nueva.");
        System.out.println("-Pulse 2 para vender una entrada.");
        System.out.println("-Pulse 3 para ver la cantidad de butacas asignadas en una sala.");
        System.out.println("-Pulse 4 para ver los datos de una persona de una butaca concreta.");
        System.out.println("-Pulse 5 para ver la matriz de butacas de una sala concreta.");
        System.out.println("-Pulse 6 para liberar una butaca.");
        System.out.println("-Pulse 7 para liberar todas las butacas de una sala. ");
        System.out.println("-Pulse 8 para cerrar el programa");
        

    }
}
