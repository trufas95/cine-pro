package proyectofinalprimeraevaluacioncine;

public class Butaca {

    private boolean ocupada;
    private Cliente cliente;  //atributo.

    public Butaca() {
        ocupada = false;
    }

    public void setCliente(Cliente clienteNuevo) { //introducimos info a un cliente.
        cliente = clienteNuevo;
        if (cliente != null) { //si cliente es distinto de null ocupada es true.
            ocupada = true;

        } else {
            ocupada = false;

        }
    }

    public Cliente getCliente() {    //devuelve cliente, accede a el.
        return cliente;
    }

    public boolean isOcupada() {  //cuando es booleano es is en vez de get.
        return ocupada;
    }

    public void setOcupada(boolean ocupada) {
        this.ocupada = ocupada;
    }

}
