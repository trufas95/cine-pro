package proyectofinalprimeraevaluacioncine;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Cine cine = new Cine();

        Scanner sc = new Scanner(System.in);

        boolean programa = true;

        while (programa == true) {

            Sala sala;
            int numeroDeSala;

            int fila;
            int columna;
            Cliente cliente;
            cine.help();
            String caso = sc.nextLine();
            switch (caso) {
                case "1":  //crear sala.
                    System.out.println("El cine dispone de 10 salas, hay: " + cine.ultimaSala + " salas ocupadas.");
                    
                    cine.crearSala();
                   
                    break;

                case "2":  //vender entrada

                    System.out.println("¿En que sala desea sacar la entrada?");
                    numeroDeSala = cine.pedirEnteroPositivo();

                    sala = cine.getSala(numeroDeSala);

                    if (sala != null) {
                        cliente = sala.crearCliente();
                        if (cliente.getEdad() >= sala.getPelicula().getEdadMinima()) { //edad minima de la pelicula, no de la sala.
                            System.out.println("Introduce fila");
                            fila = cine.pedirEnteroPositivo();

                            System.out.println("Introduce columna");
                            columna = cine.pedirEnteroPositivo();

                            sala.sacarEntrada(fila, columna, cliente);
                        } else {
                            System.out.println("No tienes edad suficiente para ver esta película.");
                        }
                    } else {
                        System.err.println("No existe bro");
                    }

                    break;
                case "3":  //Ver cantidad de butacas asignadas en una sala concreta.

                    System.out.println("¿En que sala quieres ver las butacas?");
                    numeroDeSala = cine.pedirEnteroPositivo();

                    sala = cine.getSala(numeroDeSala);
                    if (sala != null) {

                        System.out.println("Hay " + sala.butacasOcupadas() + " butacas ocupadas.");

                    } else {
                        System.err.println("No existe bro");
                    }
                    break;
                case "4":  //Ver datos de la persona que ocupa una butaca concreta.

                    System.out.println("¿En que sala desea consultar la butaca?");
                    numeroDeSala = cine.pedirEnteroPositivo();

                    sala = cine.getSala(numeroDeSala);
                    if (sala != null) {
                        System.out.println("Introduce fila");
                        fila = cine.pedirEnteroPositivo();

                        System.out.println("Introduce columna");
                        columna = cine.pedirEnteroPositivo();

                        sala.consultarCliente(fila, columna);
                    } else {
                        System.err.println("No existe bro");
                    }

                    break;

                case "5":  //Ver matriz de butacas de una sala concreta
                    System.out.println("¿De que sala desea ver las butacas libres y ocupadas?");
                    numeroDeSala = cine.pedirEnteroPositivo();

                    sala = cine.getSala(numeroDeSala);
                    if (sala != null) {

                        sala.pintarSala();

                    } else {
                        System.err.println("No existe bro");
                    }

                    break;
                case "6":  //Liberar una butaca (devolución de entrada)
                    System.out.println("¿En que sala desea liberar la entrada?");
                    numeroDeSala = cine.pedirEnteroPositivo();

                    sala = cine.getSala(numeroDeSala);
                    if (sala != null) {
                        System.out.println("Introduce fila");
                        fila = cine.pedirEnteroPositivo();

                        System.out.println("Introduce columna");
                        columna = cine.pedirEnteroPositivo();

                        sala.liberarButaca(fila, columna);
                    } else {
                        System.err.println("No existe bro");
                    }

                    break;
                case "7":  //Liberar todas las butacas de una sala

                    System.out.println("¿En que sala desea liberar TODAS las butacas?");
                    numeroDeSala = cine.pedirEnteroPositivo();

                    sala = cine.getSala(numeroDeSala);
                    if (sala != null) {

                        sala.inicializarButacas();
                        System.out.println("Has liberado todas las butacas.");

                    } else {
                        System.err.println("No existe bro");
                    }

                    break;
                case "8":  //Salir

                    System.out.println("Programa cerrado.");
                    programa = false;

                    break;
                default:
                    System.err.println("Introduzca un valor entre 1 y 8.");

                    cine.help();
                    break;

            }
        }

    }

}
