package proyectofinalprimeraevaluacioncine;

import java.util.Scanner;

public class Pelicula {

    private String nombre;
    private int edadMinima;
    private int duracion;

    public Pelicula() {

        String pelicula;

        Scanner sc = new Scanner(System.in);
        pelicula = sc.nextLine();

    }

    public String getNombre() {
        return nombre;          //getNombre nos retorna un nombre para película.
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;    
    }

    public int getEdadMinima() {
        return edadMinima;   //getedadminima devuelve la edadminima.
    }

    public void setEdadMinima(int edadMinima) { //introducimos la edad minima a una peli.
        this.edadMinima = edadMinima;
    }

    public int getDuracion() {
        return duracion; //getduracion devuelve la duracion.
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;  //declaramos setduracion como duracion e introduce datos.
    }
    
}
