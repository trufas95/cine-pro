package proyectofinalprimeraevaluacioncine;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Cliente {

    public String nombre; //String público para Cliente.
    public String DNI; //String público para Cliente.
    Date fechaNacimiento; //tipo Date.

    public void dni() {
        Scanner sc = new Scanner(System.in);
        System.out.println("DNI o NIE: ");
        DNI = sc.nextLine();
        while (DNI.length() != 9) {  //pedimos el dni o nie, deben tener un máximo de 9 digitos
            System.err.println("\"Error, DNI o NIE incorrecto, pruebe otra vez.\"");
            DNI = sc.nextLine();
        }
        System.out.println("DNI o NIE recibido. ");

    }

    public void nombre() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nombre y apellido: ");
        //El nombre debe tener mas de dos caracteres. sino dará error.

        nombre = sc.nextLine();
        while (nombre.length() < 2) {

            System.err.println("Error, escriba un nombre válido. ");
            nombre = sc.nextLine();
        }

    }

    public void fecha() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Fecha de nacimiento: ");

        System.out.println("Día: ");

        int dia = sc.nextInt();
//Dará error y pedirá de nuevo un día si es mayor de 31 o menor de 1.
        while (dia > 31 || dia < 1) {
            System.out.println("Error, introduce un día válido: ");
            dia = sc.nextInt();
        }
        System.out.println("Mes: ");
        int mes = sc.nextInt();
//Dará error si es mayor de mes 12 o menor que 1 y pedirá el dato de nuevo.
        while (mes > 12 || mes < 1) {
            System.out.println("Error, introduce un mes válido: ");
            mes = sc.nextInt();
        }

        System.out.println("Año: ");
        int anio = sc.nextInt();
//Dará error si se introduce un año menor que 1900 y pedirá el dato de nuevo.
        while (anio <= 1900) {
            System.out.println("Error, introduce un año valido: ");
            anio = sc.nextInt();
        }

        Calendar calendar = GregorianCalendar.getInstance(); //get instance es un metodo que devuelve un objeto de la clase calendar del día corriente.
        calendar.set(anio, mes, dia); //le introducimos los parámetros del día que queremos.
        fechaNacimiento = calendar.getTime(); //get time devuelve un date que mola más. //fechanacimiento está declarado arriba.

        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy"); //método predefinido que simplifica los date.

        System.out.println("Fecha de nacimiento: " + f.format(fechaNacimiento)); //clase f.format de java ya hecha para fechas bonitas.
    }

    @Override // herencia de la clase objeto.  //PREHECHO por JAVA.
    public String toString() {  //devuelve todo colocadito y chachi como una cadena.
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        return "Cliente{" + "Nombre=" + nombre + ", DNI=" + DNI + ", fechaNacimiento=" + f.format(fechaNacimiento) + '}'; //f.format para que quede bonito la fecha.
    }

    public int getEdad() {
        Calendar hoy = Calendar.getInstance();//devuelve un calendar a fecha de hoy.
        Calendar nacimiento = Calendar.getInstance();
        nacimiento.setTime(fechaNacimiento); //convierte el date de arriba a un calendar para poder operar con ello. 
        int anioActual = hoy.get(Calendar.YEAR); //get extrae un dato del calendar.
        int anioNacimiento = nacimiento.get(Calendar.YEAR); //get extrae un dato int del calendar.

        return anioActual - anioNacimiento; //así podemos operar con los datos año y calcular que nadie pase si es menor de la fecha indicada.

    }

}
