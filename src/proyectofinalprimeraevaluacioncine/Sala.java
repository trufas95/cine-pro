package proyectofinalprimeraevaluacioncine;

public class Sala {

    Butaca[][] butacas;

    final private int filasMaximas;
    final private int columnasMaximas;
    private Pelicula pelicula;

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Sala(int filasMaximas, int columnasMaximas) {
        this.filasMaximas = filasMaximas;
        this.columnasMaximas = columnasMaximas;
        butacas = new Butaca[filasMaximas][columnasMaximas];
        inicializarButacas();
    }

    public void inicializarButacas() {
        // version for normal
        for (int i = 0; i < butacas.length; i++) {
            for (int j = 0; j < butacas[i].length; j++) {
                butacas[i][j] = new Butaca();
            }//crea las butacas de la sala.
        }
    }

    public int butacasOcupadas() {
        int totalButacas = 0;

        for (int i = 0; i < butacas.length; i++) {
            for (int j = 0; j < butacas[i].length; j++) {
                Butaca butaca = butacas[i][j];  //como un for each, nos guardamos el numero de la butaca por la que vamos.
                if (butaca.isOcupada()) {
                    totalButacas = totalButacas + 1;
                }
            }
        }
        return totalButacas;
    }

    public void sacarEntrada(int filaHumana, int columnaHumana, Cliente cliente) {
        int fila = filaHumana - 1; //porque empieza en 0 y no queremos que se nos descuadre, asi se le resta uno al parametro que le damos para igualar con el array.
        int columna = columnaHumana - 1;

        if (fila >= filasMaximas || columna >= columnasMaximas) {
            System.err.println("ERROR BUTACA NO EXISTE");
        } else {
            Butaca butaca = butacas[fila][columna];

            if (butaca.isOcupada()) {
                System.err.println("ERROR BUTACA OCUPADA");
            } else {
                butaca.setCliente(cliente);
                System.out.println("Entrada vendida.");
            }
        }
    }

    public Cliente crearCliente() {
        Cliente cliente = new Cliente();
        cliente.dni();
        cliente.nombre();
        cliente.fecha();
        return cliente;

    }

    public void consultarCliente(int filaHumana, int columnaHumana) {
        int fila = filaHumana - 1;
        int columna = columnaHumana - 1;

        if (fila >= filasMaximas || columna >= columnasMaximas) {
            System.err.println("ERROR BUTACA NO EXISTE");
        } else {
            Butaca butaca = butacas[fila][columna];
            Cliente cliente = butaca.getCliente(); //el get nos devuelve el cliente de la butaca.

            if (butaca.isOcupada() == false) { //si la butaca no esta ocupada salta el error. 
                System.err.println("ERROR BUTACA VACÍA");
            } else {
                System.out.println(cliente);
            }
        }
    }

    public void liberarButaca(int filaHumana, int columnaHumana) {
        int fila = filaHumana - 1;
        int columna = columnaHumana - 1;

        if (fila >= filasMaximas || columna >= columnasMaximas) {
            System.err.println("ERROR BUTACA NO EXISTE");
        } else {
            Butaca butaca = butacas[fila][columna]; //recupera la butaca concreta.
            butaca.setCliente(null);
            System.out.println("Butaca liberada.");
        }
    }

    public void pintarSala() {

        for (int i = 0; i < butacas.length; i++) {//recorre la longitud del array principal.
            for (int j = 0; j < butacas[i].length; j++) {//recorre los demas arrays.
                Butaca butaca = butacas[i][j]; //butaca concreta .

                if (butaca.isOcupada()) {
                    System.out.print("+");

                } else {
                    System.out.print("O");
                }
            }
            System.out.println(" "); //cuando llega al final del for j imprime un salto de línea.
        }
    }

}
